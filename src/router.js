import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Signup from './views/Signup.vue';
import Login from './views/Login.vue';
import Projects from './views/Projects.vue';
import RFiles from './views/RFiles.vue';
import YourProjects from './views/YourProjects.vue';
import YourFiles from './views/YourFiles.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects,
    },
    {
      path: '/projects/:id',
      name: 'rFiles',
      component: RFiles,
    },
    {
      path: '/yourprojects',
      name: 'yourProjects',
      component: YourProjects,
    },
    {
      path: '/yourprojects/:id',
      name: 'yourFiles',
      component: YourFiles,
    },
  ],
});
