import 'bootstrap/dist/css/bootstrap.css';
import 'vue-loading-overlay/dist/vue-loading.css';

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import VueCookie from 'vue-cookie';
import Loading from 'vue-loading-overlay';
import App from './App.vue';
import router from './router';

Vue.use(Loading);
Vue.use(BootstrapVue);
Vue.use(VueCookie);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
