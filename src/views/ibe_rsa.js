import aesjs from 'aes-js';


export default {
  egcd(a, b) {
    if (a == 0) {
      return [b, BigInt(0), BigInt(1)];
    }
    else {
      let g, y, x;
      [g, y, x] = this.egcd(this.mod(b, a), a);
      return [g, x - (b / a) * y, y];
    }
  },
  modInv(a, m) {
    let g, x, y;
    [g, x, y] = this.egcd(a, m)

    if (g != 1) {
      throw Error('Modular Inverse does not exist');
    }
    else {
      return this.mod(x, m);
    }
  },
  encrypt_using_rsa(message, recipient_public_key, public_param) {    
    const messageInt = BigInt(this.intFromBytes(message)); 

    if (recipient_public_key < 0) {
      return this.modExp(
        this.modInv(messageInt, public_param), -(recipient_public_key), public_param);
    }
    return this.modExp(messageInt, recipient_public_key, public_param)
  },
  decryptUsingRSA(cipher, recipientPrivateKey, publicParam) {
    let decryptedMessageInt = BigInt(0);
    console.log(cipher, recipientPrivateKey, publicParam);

    if (recipientPrivateKey < 0) {
      console.log("a");
      decryptedMessageInt = this.modExp(
        this.modInv(cipher, publicParam), -(recipientPrivateKey), publicParam);
    } else {
      console.log("b");
      decryptedMessageInt = this.modExp(cipher, recipientPrivateKey, publicParam);
    }
    console.log("c");
    return this.bytesFromInt(decryptedMessageInt);
  },
  intFromBytes(bytes) {
    const bytesHex = this.toHexString(bytes);
    return BigInt(bytesHex);
  },
  toHexString(byteArray) {
    var s = '0x';
    byteArray.forEach(function(byte) {
      s += ('0' + (byte & 0xFF).toString(16)).slice(-2);
    });
    return s;
  },
  bytesFromInt(num) {
    const numAsHex = num.toString(16);
    return aesjs.utils.hex.toBytes(numAsHex);
  },
  modExp(a, b, n) {
      a = this.mod(a, n);
      var result = BigInt(1);
      var x = a;
      while (b > 0) {
          var leastSignificantBit = this.mod(b, BigInt(2));
          b = b / BigInt(2);
          if (leastSignificantBit == BigInt(1)) {
              result = result * x;
              result = this.mod(result, n);
          }
          x = x * x;
          x = this.mod(x, n);
      }
      return result;
  },
  mod(n, m) {
    return ((n % m) + m) % m;
  }
};

// const msgBytes = bytesFromInt(24);
// const recipient_public_key = -8280737513477076109n;
// const public_param = 18601951930157172332726417415022408279623999744714580848223765972242149262207501390614769520618117814485567494073512038932045617374951503873672111100640811441154912705035402594367425698620058758533895952449400057780506764627046114032979910819156154017026120806952504489656943200085042611160718956261587866058514100548701494157923116448096392673291544868013804423852762597225504498806631316426552051212982293945067198895136221719631075865961430160698366147171045898013887126012036527082360201998709775782383952859126550676998103535088224717441500329017904890616607348559846289686001277627340313852081947010081630379409n;

// const res = encrypt_using_rsa(msgBytes, recipient_public_key, public_param);

// const a = -775081330423215513863600725625933678317666656029774202009323582176756219258645891275615396692421575603565312253063001622168567390622979328069671295860033810048121362709808441431976070775835781605578998018725002407521115192793588084707496284131506417376088366956354353735705966670210108798363289844232827752438087522862562256580129852004016361387147702833908517660531774884396020783609638184439668800540928914377799953964009238317961494415059590029098589465460245750578630250501521961765008416612907324265998035796939611541587647295342696560062513709079370442358639523326928736916719901139179743836747792086734599142n;
// const m = 18601951930157172332726417415022408279623999744714580848223765972242149262207501390614769520618117814485567494073512038932045617374951503873672111100640811441154912705035402594367425698620058758533895952449400057780506764627046114032979910819156154017026120806952504489656943200085042611160718956261587866058514100548701494157923116448096392673291544868013804423852762597225504498806631316426552051212982293945067198895136221719631075865961430160698366147171045898013887126012036527082360201998709775782383952859126550676998103535088224717441500329017904890616607348559846289686001277627340313852081947010081630379409n;
// const res = mod(a, m);

// console.log(`(msgBytes: ${msgBytes}, recipient_public_key: ${recipient_public_key}, public_param: ${public_param}`);

// console.log(res);
