import aesjs from 'aes-js';
import aes from './aes_in_js';
import ibe from './ibe_rsa';
import axios from 'axios';


export default {
  downloadFile(url, headers, file_key, aes_key) {
    fetch(url, headers)
      .then(resp => {
        if (resp.status == 403) {
          alert("You are not allowed to download that file");
        }
        if (resp.status == 200) {
          return resp.formData();
        }
      })
      .then((formData) => {
        const file = formData.get(file_key);
        const encryptedAESKey = BigInt(formData.get(aes_key));
        console.log(`Received file with contents: ${file}`);
        console.log(file);

        const fileReader = new FileReader();

        fileReader.onload = (evt) => {
          console.log(fileReader.result);
          const fileBytes = new Uint8Array(fileReader.result);
          console.log(`fileBytes in Hex: ${aesjs.utils.hex.fromBytes(fileBytes)}`);
          this.decryptFile(encryptedAESKey, fileBytes, headers, file.name);
        };
        fileReader.readAsArrayBuffer(file);
      })
      .catch((err) => {
        console.log(err);
      });
  },
  forceFileDownload(file) {
    const url = window.URL.createObjectURL(file);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', file.name);
    document.body.appendChild(link);
    link.click();
  },
  decryptFile(encryptedAESKey, fileBytes, headers, fileName) {
    // TODO: STORE USER ID AS A COOKIE
    const path = `${process.env.VUE_APP_PKG_SERVER_BASE_URL}/private_key/rsa`;
    axios.post(path, { id: 'mwendaeddy@gmail.com' }, headers)
      .then((res) => {
        console.log(res);
        const publicParam = BigInt(res.data.public_params.rsa.n);
        const privateKey = BigInt(res.data.priv_key);

        console.log(`Private Key: ${privateKey}, Public Param: ${publicParam}`);
        // console.log();
        console.log(`ENC AES KEY: ${encryptedAESKey}`);
        
        const aesKey = ibe.decryptUsingRSA(encryptedAESKey, privateKey, publicParam);

        console.log(`aesKey: ${aesKey}`);

        const decryptedFileBytes = aes.decryptMessage(aesKey, fileBytes);
        const decryptedFile = new File([decryptedFileBytes], fileName);

        console.log(decryptedFile);

        this.forceFileDownload(decryptedFile);
      })
      .catch((err) => {console.log(err.response)});
  },
}