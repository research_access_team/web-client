// import bcrypt from 'bcrypt';
import aesjs from 'aes-js';
import uuid from 'uuid';


export default {
	generateAesKey() {
	  const saltRounds = 10;
	  const myPlaintextPassword = uuid.v4();

	  // var salt = bcrypt.genSaltSync(saltRounds);
	  // var hash = bcrypt.hashSync(myPlaintextPassword, salt);
	  
	  return aesjs.utils.utf8.toBytes(myPlaintextPassword).slice(0, 32);
	},
	encryptMessage(key, text) {
	  var textBytes = aesjs.utils.utf8.toBytes(text);
	  var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
	  
	  return aesCtr.encrypt(textBytes);
	},
	decryptMessage(key, cipherBytes) {
	  var aesCtr = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(5));
	  return aesCtr.decrypt(cipherBytes);
	},
};


// Generate Key
// const key = generateAesKey();
// const keyHex = aesjs.utils.hex.fromBytes(key);
// console.log(`Key (${key.length} bytes): ${keyHex}`);

// // Encrypt msg
// var text = 'Text may be any length you wish, no padding is required.';
// const cipher = encryptMessage(key, text);
// const cipherHex = aesjs.utils.hex.fromBytes(cipher);

// // Display cipher
// console.log(`Cipher (${cipher.length} bytes): ${cipherHex}`);
